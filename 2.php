<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Тестируем формы</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet">
    <link href="css/2.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<h1>Привет всем!</h1>
<div class="container">
    <div class="row">
        <div class="col-lg-10 m-cont">
            <h3>Контент</h3>
            <form method="get" action="2.php">
                <div class="form-group">
                    <label class="col-lg-4" for="InputEmail">Эл.почта</label>
                    <div class="col-lg-8">
                        <input type="email" class="form-control" id="InputEmail">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4" for="InputPwd">Пароль</label>
                    <div class="col-lg-8">
                        <input type="password" class="form-control" id="InputPwd">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4" for="InputFile">Загрузить файл</label>
                    <div class="col-lg-8">
                        <input type="file" class="form-control" id="InputFile">
                        <p class="help-block">Подсказка... Подсказка... Подсказка... Подсказка... </p>
                    </div>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">Кликни
                    </label>
                </div>
                <button type="submit" class="btn btn-default col-lg-4 text-right">Нажать</button>
            </form>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-left">Left aligned text.</p>
                    <p class="text-center">Center aligned text.</p>
                    <p class="text-right">Right aligned text.</p>
                    <p class="text-justify">Justified text.</p>
                    <p class="text-nowrap">No wrap text.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-2 m-menu">
            <h3>Меню</h3>
        </div>
    </div>
</div>

<script src="js/jquery/1.12.3/jquery.min.js"></script>
<script src="js/bootstrap/3.3.6/bootstrap.min.js"></script>
</body>
</html>