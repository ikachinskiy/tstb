<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap - собираем форму</title>
    <link href="css/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/3.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="//use.edgefonts.net/andika.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-2 bg-primary">
            <h3 class="bg-primary">Меню</h3>
        </div>
        <div class="col-md-7 bg-warning">
            <h1 class="bg-warning">Тест Boostrap - № 3!</h1>
            <p class="bg-warning lead">Test example. Это пример текста с новым шрифтом от Adobe.
                Это пример текста с новым шрифтом от Adobe.
                Это пример текста с новым шрифтом от Adobe. Это пример текста с новым шрифтом от Adobe. </p>
            <p class="bg-warning">Это пример текста с новым шрифтом от Adobe. Это пример текста с новым шрифтом от Adobe.
                Это пример текста с новым шрифтом от Adobe. Это пример текста с новым шрифтом от Adobe. </p>
        </div>
        <div class="col-md-3 bg-success">
            <h3 class="bg-success">Новости</h3>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery/1.12.3/jquery.min.js"></script>
<script src="js/bootstrap/3.3.6/bootstrap.min.js"></script>
</body>
</html>