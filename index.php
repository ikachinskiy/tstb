<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>tstB - 01</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <div class="container">
        <div id="banner" class="row">
            <div class="col-lg-4 logo1">
                <img  alt="#" width="200" height="200" src="images/animal-01.jpg">
            </div>
            <div class="col-lg-4 logo2">
                <img alt="#" width="200" height="200" src="images/animal-02.jpg">
            </div>
            <div class="col-lg-4 logo3">
                <img alt="#" width="200" height="200" src="images/animal-03.jpg">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 m-content">
                <label class="radio-inline">
                    <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 1
                </label>
                <label class="radio-inline">
                    <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> 2
                </label>
                <label class="radio-inline">
                    <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> 3
                </label>
                <p>Это пример текста. Это пример текста. Это пример текста. Это пример текста. Это пример текста.
                    Это пример текста. Это пример текста. Это пример текста. Это пример текста. Это пример текста.
                    Это пример текста. Это пример текста. Это пример текста. Это пример текста. Это пример текста. </p>
            </div>
            <div class="col-lg-4 r-menu">
                <ul>
                    <li>Раз</li>
                    <li>Два</li>
                    <li>Три</li>
                    <li>Четыре</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap/3.3.6/bootstrap.min.js"></script>
</body>
</html>